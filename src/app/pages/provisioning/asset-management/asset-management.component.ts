import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-asset-management',
  templateUrl: './asset-management.component.html',
  styleUrls: ['./asset-management.component.css']
})
export class AssetManagementComponent implements OnInit {


  tabs = 
  [ 
    { 
      key: 'tab-asset-management',
      value:'Asset Management'
    }
  ];

  activeTab = this.tabs[0].key;


  constructor() { }

  ngOnInit() {
  }

}
