import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabAssetManagementComponent } from './tab-asset-management.component';

describe('TabAssetManagementComponent', () => {
  let component: TabAssetManagementComponent;
  let fixture: ComponentFixture<TabAssetManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabAssetManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabAssetManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
