import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeManufacturerComponent } from './home-manufacturer.component';

describe('HomeManufacturerComponent', () => {
  let component: HomeManufacturerComponent;
  let fixture: ComponentFixture<HomeManufacturerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeManufacturerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeManufacturerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
