import { Component, OnInit } from '@angular/core';
import * as CanvasJS from '../../../../assets/js/canvasjs/canvasjs.min';

@Component({
  selector: 'app-home-manufacturer',
  templateUrl: './home-manufacturer.component.html',
  styleUrls: ['./home-manufacturer.component.css']
})
export class HomeManufacturerComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    this.chartDailyIntergrityRateByManufacturers();
    this.chartMonthlyIntergrityRateByManufacturers();
    this.chartDailyIntergrityRateBySingleMultipleManufacturers();
    this.chartMonthlyIntergrityRateBySingleMultipleManufacturers();
  }


  chartDailyIntergrityRateByManufacturers()
  {
    let chart = new CanvasJS.Chart("chart-daily-integrity-rate-by-manufacturers", {
      animationEnabled: true,
      exportEnabled: true,
      title: {
        text: ""
      },
      axisY: {
        minimum: 60,
        maximum: 100,
        interval: 10,
        suffix: "%"
      },
      data: [{
        type: "column",
        yValueFormatString: "00.00'%'",
        dataPoints: [
          { y: 99.66, label: "SinePulse" },
          { y: 100.00, label: "LandisGyr" },
          { y: 100.00, label: "Siemens" }
        ]
      }]
    });
      
    chart.render();
  }

  chartMonthlyIntergrityRateByManufacturers()
  {
    let chart = new CanvasJS.Chart("chart-monthly-integrity-rate-by-manufacturers", {
      animationEnabled: true,
      exportEnabled: true,
      title: {
        text: ""
      },
      axisY: {
        minimum: 60,
        maximum: 100,
        interval: 10,
        suffix: "%"
      },
      data: [{
        type: "column",
        yValueFormatString: "00.00'%'",
        dataPoints: [
          { y: 95.64, label: "SinePulse" },
          { y: 91.18, label: "LandisGyr" },
          { y: 91.18, label: "Siemens" }
        ]
      }]
    });
      
    chart.render();
  }

  chartDailyIntergrityRateBySingleMultipleManufacturers()
  {
    let chart = new CanvasJS.Chart("chart-daily-integrity-rate-by-single-multiple-manufacturers", {
      animationEnabled: true,
      title:{
        text: ""
      },
      axisX:{
        title: "",
        minimum: new Date(2018, 4, 10),
        maximum: new Date(2018, 4, 19),
        interval: 1,
        intervalType: "day",
        valueFormatString: "DD/MM"
      },
      axisY:{
        title:"",
        suffix: "%",
        minimum: 99.5,
        maximum: 100,
      },
      toolTip:{
        shared: true
      },
      data: [{
        type: "splineArea",
        name: "SinePulse",
        showInLegend: "true",
        xValueFormatString: "DD MMM",
        yValueFormatString: "00.00'%'",
        dataPoints: [
          { x: new Date(2018, 4, 10), y: 99.99 },
          { x: new Date(2018, 4, 11), y: 99.75 },
          { x: new Date(2018, 4, 12), y: 99.7 },
          { x: new Date(2018, 4, 13), y: 99.6 },
          { x: new Date(2018, 4, 14), y: 99.58 },
          { x: new Date(2018, 4, 15), y: 99.55 },
          { x: new Date(2018, 4, 16), y: 99.53 },
          { x: new Date(2018, 4, 17), y: 99.57 },
          { x: new Date(2018, 4, 18), y: 99.68 },
          { x: new Date(2018, 4, 19), y: 99.67 }
        ]
      },
      {
        type: "splineArea",
        name: "LandisGyr",
        showInLegend: "true",
        xValueFormatString: "DD MMM",
        yValueFormatString: "00.00'%'",
        dataPoints: [
          { x: new Date(2018, 4, 10), y: 99.99 },
          { x: new Date(2018, 4, 11), y: 99.99 },
          { x: new Date(2018, 4, 12), y: 99.99 },
          { x: new Date(2018, 4, 13), y: 99.99 },
          { x: new Date(2018, 4, 14), y: 100.00 },
          { x: new Date(2018, 4, 15), y: 100.00 },
          { x: new Date(2018, 4, 16), y: 100.00 },
          { x: new Date(2018, 4, 17), y: 100.00 },
          { x: new Date(2018, 4, 18), y: 99.99 },
          { x: new Date(2018, 4, 19), y: 99.99 }
        ]
      },
      {
        type: "splineArea",
        name: "Siemens",
        showInLegend: "true",
        xValueFormatString: "DD MMM",
        yValueFormatString: "00.00'%'",
        dataPoints: [
          { x: new Date(2018, 4, 10), y: 100.00 },
          { x: new Date(2018, 4, 11), y: 100.00 },
          { x: new Date(2018, 4, 12), y: 100.00 },
          { x: new Date(2018, 4, 13), y: 100.00 },
          { x: new Date(2018, 4, 14), y: 100.00 },
          { x: new Date(2018, 4, 15), y: 100.00 },
          { x: new Date(2018, 4, 16), y: 100.00 },
          { x: new Date(2018, 4, 17), y: 100.00 },
          { x: new Date(2018, 4, 18), y: 100.00 },
          { x: new Date(2018, 4, 19), y: 100.00 }
        ]
      }]
    });
    chart.render();
  }

  chartMonthlyIntergrityRateBySingleMultipleManufacturers()
  {
    let chart = new CanvasJS.Chart("chart-monthly-integrity-rate-by-single-multiple-manufacturers", {
      animationEnabled: true,
      title:{
        text: ""
      },
      axisX:{
        title: "",
        minimum: new Date(2017, 5),
        maximum: new Date(2018, 3),
        interval: 2,
        intervalType: "month",
        valueFormatString: "MM-YYYY"
      },
      axisY:{
        title:"",
        suffix: "%",
        minimum: 75,
        maximum: 100,
        interval: 5
      },
      toolTip:{
        shared: true
      },
      data: [{
        type: "splineArea",
        name: "SinePulse",
        showInLegend: "true",
        xValueFormatString: "MMM YYYY",
        yValueFormatString: "00.00'%'",
        dataPoints: [
          { x: new Date(2017, 5), y: 87.00 },
          { x: new Date(2017, 7), y: 87.00 },
          { x: new Date(2017, 9), y: 92.00 },
          { x: new Date(2017, 11), y: 91.00 },
          { x: new Date(2018, 1), y: 100.00 },
          { x: new Date(2018, 3), y: 96.00 }
        ]
      },
      {
        type: "splineArea",
        name: "LandisGyr",
        showInLegend: "true",
        xValueFormatString: "DD MMM",
        yValueFormatString: "00.00'%'",
        dataPoints: [
          { x: new Date(2017, 5), y: 77.00 },
          { x: new Date(2017, 7), y: 77.00 },
          { x: new Date(2017, 9), y: 85.00 },
          { x: new Date(2017, 11), y: 84.00 },
          { x: new Date(2018, 1), y: 100.00 },
          { x: new Date(2018, 3), y: 96.00 }
        ]
      },
      {
        type: "splineArea",
        name: "Siemens",
        showInLegend: "true",
        xValueFormatString: "DD MMM",
        yValueFormatString: "00.00'%'",
        dataPoints: [
          { x: new Date(2017, 5), y: 80.00 },
          { x: new Date(2017, 7), y: 80.00 },
          { x: new Date(2017, 9), y: 88.00 },
          { x: new Date(2017, 11), y: 87.00 },
          { x: new Date(2018, 1), y: 100.00 },
          { x: new Date(2018, 3), y: 99.99 }
        ]
      }]
    });
    chart.render();
  }

}
