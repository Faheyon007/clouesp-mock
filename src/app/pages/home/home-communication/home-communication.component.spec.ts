import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeCommunicationComponent } from './home-communication.component';

describe('HomeCommunicationComponent', () => {
  let component: HomeCommunicationComponent;
  let fixture: ComponentFixture<HomeCommunicationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeCommunicationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeCommunicationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
