import { Component, OnInit } from '@angular/core';
import * as CanvasJS from '../../../../assets/js/canvasjs/canvasjs.min';

@Component({
  selector: 'app-home-model',
  templateUrl: './home-model.component.html',
  styleUrls: ['./home-model.component.css']
})
export class HomeModelComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    this.chartDailyIntergrityRateByModels();
    this.chartMonthlyIntergrityRateByModels();
    this.chartDailyIntergrityRateBySingleMultipleModels();
    this.chartMonthlyIntergrityRateBySingleMultipleModels();
  }


  chartDailyIntergrityRateByModels()
  {
    let chart = new CanvasJS.Chart("chart-daily-integrity-rate-by-models", {
      animationEnabled: true,
      exportEnabled: true,
      title: {
        text: "Basic Column Chart in Angular"
      },
      data: [{
        type: "column",
        dataPoints: [
          { y: 71, label: "Apple" },
          { y: 55, label: "Mango" },
          { y: 50, label: "Orange" },
          { y: 65, label: "Banana" },
          { y: 95, label: "Pineapple" },
          { y: 68, label: "Pears" },
          { y: 28, label: "Grapes" },
          { y: 34, label: "Lychee" },
          { y: 14, label: "Jackfruit" }
        ]
      }]
    });
      
    chart.render();
  }

  chartMonthlyIntergrityRateByModels()
  {
    let chart = new CanvasJS.Chart("chart-monthly-integrity-rate-by-models", {
      animationEnabled: true,
      exportEnabled: true,
      title: {
        text: "Basic Column Chart in Angular"
      },
      data: [{
        type: "column",
        dataPoints: [
          { y: 71, label: "Apple" },
          { y: 55, label: "Mango" },
          { y: 50, label: "Orange" },
          { y: 65, label: "Banana" },
          { y: 95, label: "Pineapple" },
          { y: 68, label: "Pears" },
          { y: 28, label: "Grapes" },
          { y: 34, label: "Lychee" },
          { y: 14, label: "Jackfruit" }
        ]
      }]
    });
      
    chart.render();
  }

  chartDailyIntergrityRateBySingleMultipleModels()
  {
    let chart = new CanvasJS.Chart("chart-daily-integrity-rate-by-single-multiple-models", {
      animationEnabled: true,
      title:{
        text: "Products Sold by XYZ Ltd. in 2016"
      },
      axisX:{
        title: "Seasons",
        minimum: -0.02,
        maximum: 3.02
      },
      axisY:{
        title:"Sales"
      },
      toolTip:{
        shared: true
      },
      data: [{
        type: "stackedArea100",
        name: "Mosquito Repellents",
        showInLegend: "true",
        dataPoints: [
          { y: 83450 , label: "Spring" },
          { y: 51240, label: "Summer" },
          { y: 64120, label: "Autumn" },
          { y: 71450, label: "Fall" }
        ]
      },
      {
        type: "stackedArea100",
        name: "Liquid Soap",
        showInLegend: "true",
        dataPoints: [
          { y: 20140 , label: "Spring" },
          { y: 30170, label: "Summer" },
          { y: 24410, label: "Autumn" },
          { y: 38120, label: "Fall" }
        ]
      },
      {
        type: "stackedArea100",
        name: "Napkins",
        showInLegend: "true",
        dataPoints: [
          { y: 45120 , label: "Spring" },
          { y: 50350, label: "Summer" },
          { y: 48410, label: "Autumn" },
          { y: 53120, label: "Fall" }
        ]
      }]
    });
    chart.render();
  }

  chartMonthlyIntergrityRateBySingleMultipleModels()
  {
    let chart = new CanvasJS.Chart("chart-monthly-integrity-rate-by-single-multiple-models", {
      animationEnabled: true,
      title:{
        text: "Products Sold by XYZ Ltd. in 2016"
      },
      axisX:{
        title: "Seasons",
        minimum: -0.02,
        maximum: 3.02
      },
      axisY:{
        title:"Sales"
      },
      toolTip:{
        shared: true
      },
      data: [{
        type: "stackedArea100",
        name: "Mosquito Repellents",
        showInLegend: "true",
        dataPoints: [
          { y: 83450 , label: "Spring" },
          { y: 51240, label: "Summer" },
          { y: 64120, label: "Autumn" },
          { y: 71450, label: "Fall" }
        ]
      },
      {
        type: "stackedArea100",
        name: "Liquid Soap",
        showInLegend: "true",
        dataPoints: [
          { y: 20140 , label: "Spring" },
          { y: 30170, label: "Summer" },
          { y: 24410, label: "Autumn" },
          { y: 38120, label: "Fall" }
        ]
      },
      {
        type: "stackedArea100",
        name: "Napkins",
        showInLegend: "true",
        dataPoints: [
          { y: 45120 , label: "Spring" },
          { y: 50350, label: "Summer" },
          { y: 48410, label: "Autumn" },
          { y: 53120, label: "Fall" }
        ]
      }]
    });
    chart.render();
  }

}