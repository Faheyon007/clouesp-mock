import { Component, OnInit } from '@angular/core';
import * as CanvasJS from '../../../../assets/js/canvasjs/canvasjs.min';

@Component({
  selector: 'app-home-overview',
  templateUrl: './home-overview.component.html',
  styleUrls: ['./home-overview.component.css']
})
export class HomeOverviewComponent implements OnInit {

  cards =
  [
    {
      heading: 'Completed',
      title: '59,746',
      subtitle: '99.89%',
      footer: 'Details'
    },

    {
      heading: 'Partial',
      title: '3',
      subtitle: '0.00%',
      footer: 'Details'
    },

    {
      heading: 'Failed',
      title: '87',
      subtitle: '0.08%',
      footer: 'Details'
    }
  ];

  cube = { body: '99.64' };

  constructor() { }

  ngOnInit() {
    this.chartDailyMeterReadsIntegrityRate();
    this.chartMonthlyMeterReadsIntegrityRate();
  }


  // chartDailyMeterReadsIntegrityRate()
  // {
  //   let chart = new CanvasJS.Chart("chart-daily-meter-reads-integrity-rate", {
  //     animationEnabled: true,
  //     title:{
  //       text: ""
  //     },
  //     axisX:{
  //       title: "",
  //       minimum: 0.00,
  //       maximum: 7.00
  //     },
  //     axisY:{
  //       title:"Percentage (%)"
  //     },
  //     toolTip:{
  //       shared: true
  //     },
  //     data: [{
  //       type: "stackedArea100",
  //       name: "",
  //       showInLegend: "false",
  //       dataPoints: [
  //         { y: 99.94 , label: "05/10" },
  //         { y: 99.79, label: "05/11" },
  //         { y: 99.75, label: "05/12" },
  //         { y: 99.67, label: "05/13" },
  //         { y: 99.66 , label: "05/14" },
  //         { y: 99.25, label: "05/15" },
  //         { y: 99.10, label: "05/16" },
  //         { y: 99.58, label: "05/17" }
  //       ]
  //     }]
  //   });

  //   chart.render();
  // }

  chartDailyMeterReadsIntegrityRate()
  {
    let chart = new CanvasJS.Chart("chart-daily-meter-reads-integrity-rate", {
      animationEnabled: true,
      title:{
        text: ""
      },
      axisX:{
        title: "",
        minimum: new Date(2018, 0, 1),
        maximum: new Date(2018, 0, 9),
        interval: 1,
        intervalType: "day",
        valueFormatString: "DD/MM"
      },
      axisY :{
        includeZero: true,
        prefix: "",
        suffix: "%",
        minimum: 0,
        maximum: 100,
        viewportMinimum: 0,
        viewportMaximum: 120,
        interval: 20
      },
      toolTip:{
        shared: true
      },
      data: [{
        type: "splineArea",
        name: "",
        showInLegend: "false",
        yValueFormatString: "00.00'%'",
        xValueFormatString: "DD-MMM",
        dataPoints: [
          { x: new Date(2018, 0, 1), y: 99.91 },
          { x: new Date(2018, 0, 2), y: 99.85 },
          { x: new Date(2018, 0, 3), y: 99.79 },
          { x: new Date(2018, 0, 4), y: 99.77 },
          { x: new Date(2018, 0, 5), y: 99.56 },
          { x: new Date(2018, 0, 6), y: 99.10 },
          { x: new Date(2018, 0, 7), y: 98.94 },
          { x: new Date(2018, 0, 8), y: 99.81 },
          { x: new Date(2018, 0, 9), y: 99.94 }
        ]
      }]
    });

    chart.render();
  }

  chartMonthlyMeterReadsIntegrityRate()
  {
    let chart = new CanvasJS.Chart("chart-monthly-meter-reads-integrity-rate", {
      animationEnabled: true,
      title:{
        text: ""
      },
      axisX: {
        interval: 2,
        intervalType: "month",
        minimum: new Date(2017, 0),
        maximum: new Date(2018, 0)
      },
      axisY :{
        includeZero: true,
        prefix: "",
        suffix: "%",
        minimum: 0,
        maximum: 100,
        viewportMinimum: 0,
        viewportMaximum: 120,
        interval: 20
      },
      toolTip: {
        shared: true
      },
      legend: {
        fontSize: 13
      },
      data: [{
        type: "splineArea",
        showInLegend: false,
        name: "",
        yValueFormatString: "00.00'%'",
        xValueFormatString: "MMM YYYY",
        dataPoints: [
          { x: new Date(2017, 0), y: 82.10 },
          { x: new Date(2017, 2), y: 84.40 },
          { x: new Date(2017, 4), y: 89.77 },
          { x: new Date(2017, 6), y: 92.26 },
          { x: new Date(2017, 8), y: 96.68 },
          { x: new Date(2017, 10), y: 98.30 },
          { x: new Date(2018, 0), y: 99.99 }
        ]
       }]
    });
    chart.render();
  }

}
