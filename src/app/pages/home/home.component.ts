import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  tabs = 
  [ 
    { 
      key: 'overview',
      value:'Overview'
    },
    { 
      key: 'manufacturer',
      value: 'Manufacturers'
    },
    {
      key:'model',
      value: 'Models'
    },
    {
      key: 'communication',
      value: 'Communications'
    }
  ];
  
  activeTab = this.tabs[0].value;


  constructor() { }

  ngOnInit() {
  }

  isActiveTab(tab)
  {
    return this.activeTab == tab.value;
  }

}
