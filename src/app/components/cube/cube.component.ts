import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'cube',
  templateUrl: './cube.component.html',
  styleUrls: ['./cube.component.css']
})
export class CubeComponent implements OnInit {
  @Input('body') panelBody = "";
  
  constructor() { }

  ngOnInit() {
  }

}
