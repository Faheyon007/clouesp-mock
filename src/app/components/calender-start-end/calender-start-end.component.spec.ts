import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalenderStartEndComponent } from './calender-start-end.component';

describe('CalenderStartEndComponent', () => {
  let component: CalenderStartEndComponent;
  let fixture: ComponentFixture<CalenderStartEndComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalenderStartEndComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalenderStartEndComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
