import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {
  @Input('heading') panelHeading = "";
  @Input('title') panelTitle = "";
  @Input('subtitle') panelSubtitle = "";
  @Input('footer') panelFooter = "";
  
  
  
  constructor() { }

  ngOnInit() {
  }



}
