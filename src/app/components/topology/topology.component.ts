import { Component, OnInit } from '@angular/core';
import * as cytoscape from 'cytoscape';

@Component({
  selector: 'topology',
  templateUrl: './topology.component.html',
  styleUrls: ['./topology.component.css']
})
export class TopologyComponent implements OnInit
{
  constructor() { }

  private gridDcu;

  meters = [];
  dcus = [];
  edges = [];
 
  ngOnInit()
  {
  }

  renderDcuGrid()
  {
    this.gridDcu = cytoscape({

      container: document.getElementById('gridDcu'), // container to render in
    
      elements: [],
    
      style: [ // the stylesheet for the graph
        {
          selector: 'node',
          style: {
            'background-color': 'rgb( 255,255,100 )',
            'label': 'data(id)'
          }
        },
    
        {
          selector: 'edge',
          style: {
            'width': 3,
            'line-color': '#ccc',
            'target-arrow-color': '#ccc',
            'target-arrow-shape': 'triangle'
          }
        }
      ],
    
      layout: {
        name: 'grid',
        rows: 1
      }
    
    });

    this.gridDcu.add(this.meters);
    this.gridDcu.add(this.dcus);
    this.gridDcu.add(this.edges);
  }



  updateGridDcu(dcuId)
  {
    switch(dcuId)
    {
      case "541610063550":

        this.meters = [
          { group: "nodes", data: { id: "m00" }, position: { x: 100, y: 0 } },
          { group: "nodes", data: { id: "m01" }, position: { x: -100, y: 0 } },
          { group: "nodes", data: { id: "m02" }, position: { x: 0, y: 100 } },
          { group: "nodes", data: { id: "m03" }, position: { x: 0, y: -100 } },
          { group: "nodes", data: { id: "m04" }, position: { x: 100, y: 100 } },
          { group: "nodes", data: { id: "m05" }, position: { x: 100, y: -100 } },
          { group: "nodes", data: { id: "m06" }, position: { x: -100, y: 100 } },
          { group: "nodes", data: { id: "m07" }, position: { x: -100, y: -100 } }
        ];
  
        this.dcus = [
          { group: "nodes", data: { id: "d0"}, position: { x: 0, y:0 } }
        ];
  
        this.edges = [
          { group: "edges", data: { id: "e00", source: "d0", target: "m00" } },
          { group: "edges", data: { id: "e01", source: "d0", target: "m01" } },
          { group: "edges", data: { id: "e02", source: "d0", target: "m02" } },
          { group: "edges", data: { id: "e03", source: "d0", target: "m03" } },
          { group: "edges", data: { id: "e04", source: "d0", target: "m04" } },
          { group: "edges", data: { id: "e05", source: "d0", target: "m05" } },
          { group: "edges", data: { id: "e06", source: "d0", target: "m06" } },
          { group: "edges", data: { id: "e07", source: "d0", target: "m07" } }
        ];
      break;

      case "541610063590":
      
        this.meters = [
          { group: "nodes", data: { id: "m10" }, position: { x: 100, y: 0 } },
          { group: "nodes", data: { id: "m11" }, position: { x: -100, y: 0 } },
          { group: "nodes", data: { id: "m12" }, position: { x: 0, y: 100 } },
          { group: "nodes", data: { id: "m13" }, position: { x: 0, y: -100 } },
          { group: "nodes", data: { id: "m14" }, position: { x: 100, y: 100 } },
          { group: "nodes", data: { id: "m15" }, position: { x: 100, y: -100 } },
        ];
  
        this.dcus = [
          { group: "nodes", data: { id: "d1"}, position: { x: 0, y:0 } }
        ];
  
        this.edges = [
          { group: "edges", data: { id: "e10", source: "d1", target: "m10" } },
          { group: "edges", data: { id: "e11", source: "d1", target: "m11" } },
          { group: "edges", data: { id: "e12", source: "d1", target: "m12" } },
          { group: "edges", data: { id: "e13", source: "d1", target: "m13" } },
          { group: "edges", data: { id: "e14", source: "d1", target: "m14" } },
          { group: "edges", data: { id: "e15", source: "d1", target: "m15" } },
        ];
      break;
      
      case "541610063630":
      
        this.meters = [
          { group: "nodes", data: { id: "m20" }, position: { x: 100, y: 0 } },
          { group: "nodes", data: { id: "m21" }, position: { x: -100, y: 0 } },
          { group: "nodes", data: { id: "m22" }, position: { x: 0, y: 100 } },
          { group: "nodes", data: { id: "m23" }, position: { x: 0, y: -100 } },
          { group: "nodes", data: { id: "m24" }, position: { x: 100, y: 100 } },
          { group: "nodes", data: { id: "m25" }, position: { x: 100, y: -100 } },
          { group: "nodes", data: { id: "m26" }, position: { x: -100, y: 100 } },
          { group: "nodes", data: { id: "m27" }, position: { x: -100, y: -100 } },
          { group: "nodes", data: { id: "m28" }, position: { x: 200, y: 100 } },
          { group: "nodes", data: { id: "m29" }, position: { x: 200, y: -100 } },
          { group: "nodes", data: { id: "m30" }, position: { x: -100, y: 200 } },
          { group: "nodes", data: { id: "m31" }, position: { x: -100, y: -200 } },
          { group: "nodes", data: { id: "m32" }, position: { x: 300, y: 100 } },
          { group: "nodes", data: { id: "m33" }, position: { x: 100, y: -300 } },
          { group: "nodes", data: { id: "m34" }, position: { x: -100, y: 400 } },
          { group: "nodes", data: { id: "m35" }, position: { x: 400, y: -300 } }
        ];

        this.dcus = [
          { group: "nodes", data: { id: "d2"}, position: { x: 0, y:0 } }
        ];

        this.edges = [
          { group: "edges", data: { id: "e20", source: "d2", target: "m20" } },
          { group: "edges", data: { id: "e21", source: "d2", target: "m21" } },
          { group: "edges", data: { id: "e22", source: "d2", target: "m22" } },
          { group: "edges", data: { id: "e23", source: "d2", target: "m23" } },
          { group: "edges", data: { id: "e24", source: "d2", target: "m24" } },
          { group: "edges", data: { id: "e25", source: "d2", target: "m25" } },
          { group: "edges", data: { id: "e26", source: "d2", target: "m26" } },
          { group: "edges", data: { id: "e27", source: "d2", target: "m27" } },
          { group: "edges", data: { id: "e28", source: "d2", target: "m28" } },
          { group: "edges", data: { id: "e29", source: "d2", target: "m29" } },
          { group: "edges", data: { id: "e30", source: "d2", target: "m30" } },
          { group: "edges", data: { id: "e31", source: "d2", target: "m31" } },
          { group: "edges", data: { id: "e32", source: "d2", target: "m32" } },
          { group: "edges", data: { id: "e33", source: "d2", target: "m33" } },
          { group: "edges", data: { id: "e34", source: "d2", target: "m34" } },
          { group: "edges", data: { id: "e35", source: "d2", target: "m35" } }
        ];
      break;

      case "":
        this.meters = [];
        this.edges = [];
        this.dcus = [];
      break;
    }

    this.renderDcuGrid();
  }
  

}
