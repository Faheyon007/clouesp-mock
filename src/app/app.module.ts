import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { HomeOverviewComponent } from './pages/home/home-overview/home-overview.component';
import { CardComponent } from './components/card/card.component';
import { CubeComponent } from './components/cube/cube.component';
import { CalenderStartEndComponent } from './components/calender-start-end/calender-start-end.component';
import { CalenderComponent } from './components/calender/calender.component';
import { HomeManufacturerComponent } from './pages/home/home-manufacturer/home-manufacturer.component';
import { HomeModelComponent } from './pages/home/home-model/home-model.component';
import { HomeCommunicationComponent } from './pages/home/home-communication/home-communication.component';

import { NgxGaugeModule } from 'ngx-gauge';
import { AssetManagementComponent } from './pages/provisioning/asset-management/asset-management.component';
import { TabAssetManagementComponent } from './pages/provisioning/asset-management/tab-asset-management/tab-asset-management.component';
import { GraphComponent } from './components/graph/graph.component';

import { CytoscapeModule } from 'ngx-cytoscape';
import { DeviceListComponent } from './components/device-list/device-list.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TopologyComponent } from './components/topology/topology.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HomeOverviewComponent,
    CardComponent,
    CubeComponent,
    CalenderStartEndComponent,
    CalenderComponent,
    HomeManufacturerComponent,
    HomeModelComponent,
    HomeCommunicationComponent,
    AssetManagementComponent,
    TabAssetManagementComponent,
    GraphComponent,
    DeviceListComponent,
    TopologyComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    NgxGaugeModule,
    CytoscapeModule,
    NgbModule
  ],
  providers: [
    Title
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
