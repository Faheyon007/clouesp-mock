import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'SinePulse Head End System';
  version = 'v1.0';

  menus = 
  [
    {
      title: 'Home',
      submenus: []
    },
    {
      title: 'Data Collection',
      submenus: 
      [
        'Schedule Reads Report',
        'Miss Data Tracing',
        'Meter Data Report',
        'Meter Event Report',
        'Collection Scheme Management'
      ]
    },
    {
      title: 'Provisioning',
      submenus:
      [
        'Asset Management',
        'Meter Group Management',
        'Meter Configuration'
      ]
    },
    {
      title: 'Integration',
      submenus:
      [
        'Service Request Job Tracing',
        'Meter Data & Event Export',
        'On Demand Reads'
      ]
    },
    {
      title: 'Tools',
      submenus:
      [
        'Firmware Upgrade',
        'Meter Group Upgrade',
        'Connect / Disconnect'
      ]
    },
    {
      title: 'System',
      submenus:
      [
        'Log Explorer',
        'Deployment & Cluster Management'
      ]
    }
  ];

  activeMenu = 
  {
    title: "",
    selected: false
  }



  constructor(private serviceTitle: Title)
  {

  }


  ngOnInit()
  {
    this.setTitle(this.title + ' v' + this.version);
  }


  setTitle(title: string)
  {
    this.serviceTitle.setTitle(title);
  }
}
